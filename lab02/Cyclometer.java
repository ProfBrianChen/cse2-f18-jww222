///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 7 September 2018
/// This file outputs time and distance traveled during 2 trips on a bicycle

//
public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {
         // declares the input variables as integers and assigns value 
        int secsTrip1 = 480;  // declares seconds in trip 1 as an integer and assigns as 480 
       	int secsTrip2 = 3220;  // declares seconds in trip 2 as an integer and assigns as 3220
		    int countsTrip1 = 1561;  // declares front wheel rotations in trip 1 as an integer and assigns as 1561 
	    	int countsTrip2 = 9037; // declares front wheel rotations in trip 2 as an integer and assigns as 9037
      
        // declares and assigns inputs necessary to calculating distance and time 
        double wheelDiameter = 27.0; // diameter of a wheel is 27 inches
    	  double PI = 3.14159; // PI value used in calculating circumference declared as double 
  	    double feetPerMile = 5280; 
    	  double inchesPerFoot = 12;   
    	  double secondsPerMinute = 60;  
        double distanceTrip1, distanceTrip2,totalDistance;


        // prints Trip 1 and 2's time and rotation count to terminal 
        System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1 + " counts.");
	      System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2 + " counts.");
      
        // Calculates distance travled in the trips using PI, wheelDiameter, countsTrip2, inchesPerFoot, countsTrip1, and feetPerMile
        distanceTrip1 = countsTrip1 * wheelDiameter * PI;
    	  // gives distance in inches
    	  // for every rotation the wheel travels a total distance of wheelDiameter * PI  
      	distanceTrip1 /= inchesPerFoot * feetPerMile; // Gives distance of trip 1 in miles 
	      distanceTrip2 = countsTrip2 * wheelDiameter * PI / inchesPerFoot / feetPerMile; // gives distance of trip 2 in miles 
	      totalDistance = distanceTrip1 + distanceTrip2; // adds two trip distances for a totalDistance
      
        //Print out the output data.
        System.out.println("Trip 1 was " + distanceTrip1 + " miles");
	      System.out.println("Trip 2 was " + distanceTrip2 + " miles");
      	System.out.println("The total distance was " + totalDistance + " miles");
    
      
      
	}  //end of main method   
} //end of class
