///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 21 September 2018
/// This program generates a random number then denotes which suit and type of card that number corresponds with 
public class CardGenerator{
 public static void main(String[]args){ 
  double randNum;
  String cardSuit;
  String cardIdentity;
  randNum = Math.random()*52-1; //generates random number between 1 and 52 inclusive
   randNum = (int) randNum;
  
   // assigns the string cardSuit with the corresponding Suit for each number range 
   if ((randNum >= 1) && (randNum <= 13)){
     cardSuit = "Diamonds";
   }
   else if ((randNum >= 14) && (randNum <= 26)){
     cardSuit = "Clubs";
   }
   else if((randNum >= 27) && (randNum <= 39)){
     cardSuit = "Hearts";
   }
   else{
     cardSuit = "Spades";
   }
     
 randNum = randNum % 14; // Will get remainder number between 1 and 13 of the number 
 int e = (int) randNum;  
   // Swtiches the number of card with the corresponding identity
     switch (e){
       case 1:
         cardIdentity = "Ace";
         break; 
       case 2:
         cardIdentity = "2";
         break;
       case 3:
         cardIdentity = "3"; 
         break;
       case 4:  
         cardIdentity = "4";
         break;
       case 5:
         cardIdentity = "5";
         break;
       case 6:
         cardIdentity = "6";
         break;
       case 7:
         cardIdentity = "7";
         break;
       case 8:
         cardIdentity = "8";
         break;
       case 9:
         cardIdentity = "9";
         break;
       case 10:
         cardIdentity = "10";
         break;
       case 11:
         cardIdentity = "Jack";
         break;
       case 12:   
         cardIdentity = "Queen";
         break;
       case 13:
         cardIdentity = "King";
         break;
       default:
         cardIdentity = "unknown";
     }
   
   System.out.println("You picked the " + cardIdentity + " of " + cardSuit);
   
   
  
  
  
  
 }
}