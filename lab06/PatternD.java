///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 12 October 2018
/// This program creates a pattern of numbers at a given user length   
import java.util.Scanner;

public class PatternD{
 public static void main(String[]args){ 
   Scanner scnr = new Scanner(System.in);
   
   System.out.println("Please input an integer between 1 and 10 to indicate how many rows the pattern should be.");
   boolean userL = scnr.hasNextInt();
   
   // check to make sure it's an integer
   while (!userL){
     scnr.next();
     System.out.println("You have not input an integer, please try again.");
     userL = scnr.hasNextInt(); 
   }
   int length = scnr.nextInt(); 
   
   //check to make sure it's between 1 and 10 
   while ((length < 1) || (length > 10)){
     System.out.println("You have not input an integer between 1 and 10. Please try again.");
     length = scnr.nextInt();
   }
   
   // Write first for-loop to determine # of rows 
   for (int i = length; i >=1; i--){
     for (int g = i; g>=1; g--){
       System.out.print(g + " ");
     }// ends second for-loop 
    System.out.println(" "); 
   } // ends first for-loop
   
 } // ends main method
 
} // ends public class
   
     