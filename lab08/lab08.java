///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 13 November 2018
/// This program generates an array with a size of 100, fills this array with random integers from 0-99, then uses another array to 
/// count then print the number of occurences of each random integer from 0-99

import java.util.Random;
import java.util.Arrays;
public class lab08{
  public static void main(String[]args){
    Random numGen = new Random();
    int[]arrayOne = new int[100];
    int[]arrayTwo = new int[100];
    
    // create for loop that will assign random numbers from 0-99
    for(int i = 0; i< arrayOne.length; i++){
      int f = numGen.nextInt(100);
      arrayOne[i] = f;
    }
    for(int j = 0; j < arrayOne.length; j++){
      for(int k = 0; k < arrayOne.length; k++){
         if(arrayOne[k] == j){
           arrayTwo[j] +=1;    
         }
      }
      }
      // print out the first array
      System.out.println("Array 1 holds the following integers: " + Arrays.toString(arrayOne));
    
      // array to print the number of occurences of each number
      for(int g = 0; g < arrayTwo.length; g++){
        System.out.println(g + " occurs " + arrayTwo[g] + " times.");
      }
  } // ends main method
} // ends class
