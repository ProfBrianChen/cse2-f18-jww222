///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 16 November 2018
/// This program allows 2 users to play a game of tic tac toe
import java.util.Scanner;
public class HW10{
  public static void main(String[]args){
    String [][] ticTac = new String[3][3];
    int counter = 1;
    for(int i=0; i<ticTac.length;i++){ // iterates through for row length
      for(int j=0; j<ticTac[i].length; j++){ // iterates through each column
        ticTac[i][j] = (" " + counter); // fills array from 1-9 with values 
        counter++;
      }
    }
    printArray(ticTac);
    boolean winner = false; // create boolean winner condition 
    for(int i=1; i<=9; i++){
      if(i%2==1){
        player1(ticTac);
        printArray(ticTac);
        winner = isWinner(ticTac);
        if(winner == true){
          System.out.println("Player 1 has won!");
          break;
        }
      }
      if(i%2==0){
        player2(ticTac);
        printArray(ticTac);
        winner = isWinner(ticTac);
        if(winner == true){
          System.out.println("Player 2 has won!");
          break;
        }
      }
    }
    if(!winner){
      System.out.println("There was a draw"); 
    }
  } // ends main method
  public static void printArray(String[][]array){
    for(int i=0; i<array.length;i++){ // iterates through for row length
      for(int j=0; j<array[i].length; j++){ // iterates through each column
        System.out.print(array[i][j]+ " ");
      } // ends second for loop
      System.out.println(); // prints out second row on a new line
    } // ends first for loop
  } // ends second for loop
  
  public static String[][] player1(String[][]array){
    Scanner scan = new Scanner(System.in);
    int row = 5;
    int column = 5;
    System.out.println("Player 1, please input which # position you want to place an 'o' in as an integer.");
    boolean isInt = scan.hasNextInt();
    while(!isInt){
      System.out.println("That is not an integer. Please try again");
      scan.next();
      isInt = scan.hasNextInt();
    }
    int userPos = scan.nextInt(); 
    while((userPos < 1) || (userPos > 9)){
      System.out.println("That is not a valid position. Please try again.");
      isInt = scan.hasNextInt();
      while(!isInt){
        System.out.println("That is not an integer. Please try again");
        scan.next();
        isInt = scan.hasNextInt();
      }
      userPos = scan.nextInt();
    } // ends check for proper range 
    if((userPos>0) && (userPos<4)){
      row = 0;
    }
    if((userPos>3) && (userPos < 7)){
      row = 1; 
    }
    if((userPos > 6) && (userPos <10)){
      row = 2; 
    }
    if((userPos == 1) || (userPos == 4) || (userPos == 7)){
      column = 0; 
    }
    if((userPos == 2) || (userPos == 5) || (userPos == 8)){
      column = 1; 
    }
    if((userPos == 3) || (userPos == 6) || (userPos == 9)){
      column = 2; 
    }
    while((array[row][column].equals(" x")) || (array[row][column].equals(" o"))){ // while that position is filled, ask for a new position
      System.out.println("That position is already filled. Please try another");
      // go through all of the checks again to make sure they're not inputting something that we don't want
      isInt = scan.hasNextInt();
    while(!isInt){
      System.out.println("That is not an integer. Please try again");
      scan.next();
      isInt = scan.hasNextInt();
    }
    userPos = scan.nextInt(); 
    while((userPos < 1) || (userPos > 9)){
      System.out.println("That is not a valid position. Please try again.");
      isInt = scan.hasNextInt();
      while(!isInt){
        System.out.println("That is not an integer. Please try again");
        scan.next();
        isInt = scan.hasNextInt();
      }
      userPos = scan.nextInt();
    } // ends check for proper range 
    if((userPos>0) && (userPos<4)){
      row = 0;
    }
    if((userPos>3) && (userPos < 7)){
      row = 1; 
    }
    if((userPos > 6) && (userPos <10)){
      row = 2; 
    }
    if((userPos == 1) || (userPos == 4) || (userPos == 7)){
      column = 0; 
    }
    if((userPos == 2) || (userPos == 5) || (userPos == 8)){
      column = 1; 
    }
    if((userPos == 3) || (userPos == 6) || (userPos == 9)){
      column = 2; 
    }
    
    } // ends while loop checking if the position is filled up 
    // if they entered a valid position, fill it with an "o" 
    array[row][column] = " o";
    return array;
  }
  
  public static String[][] player2(String[][]array){
    Scanner scan = new Scanner(System.in);
    int row = 5;
    int column = 5;
    System.out.println("Player 2, please input which # position you want to place an 'x' in as an integer.");
    boolean isInt = scan.hasNextInt();
    while(!isInt){
      System.out.println("That is not an integer. Please try again");
      scan.next();
      isInt = scan.hasNextInt();
    }
    int userPos = scan.nextInt(); 
    while((userPos < 1) || (userPos > 9)){
      System.out.println("That is not a valid position. Please try again.");
      isInt = scan.hasNextInt();
      while(!isInt){
        System.out.println("That is not an integer. Please try again");
        scan.next();
        isInt = scan.hasNextInt();
      }
      userPos = scan.nextInt();
    } // ends check for proper range 
    if((userPos>0) && (userPos<4)){
      row = 0;
    }
    if((userPos>3) && (userPos < 7)){
      row = 1; 
    }
    if((userPos > 6) && (userPos <10)){
      row = 2; 
    }
    if((userPos == 1) || (userPos == 4) || (userPos == 7)){
      column = 0; 
    }
    if((userPos == 2) || (userPos == 5) || (userPos == 8)){
      column = 1; 
    }
    if((userPos == 3) || (userPos == 6) || (userPos == 9)){
      column = 2; 
    }
    while((array[row][column].equals(" x")) || (array[row][column].equals(" o"))){ // while that position is filled, ask for a new position
      System.out.println("That position is already filled. Please try another");
      // go through all of the checks again to make sure they're not inputting something that we don't want
      isInt = scan.hasNextInt();
    while(!isInt){
      System.out.println("That is not an integer. Please try again");
      scan.next();
      isInt = scan.hasNextInt();
    }
    userPos = scan.nextInt(); 
    while((userPos < 1) || (userPos > 9)){
      System.out.println("That is not a valid position. Please try again.");
      isInt = scan.hasNextInt();
      while(!isInt){
        System.out.println("That is not an integer. Please try again");
        scan.next();
        isInt = scan.hasNextInt();
      }
      userPos = scan.nextInt();
    } // ends check for proper range 
    if((userPos>0) && (userPos<4)){
      row = 0;
    }
    if((userPos>3) && (userPos < 7)){
      row = 1; 
    }
    if((userPos > 6) && (userPos <10)){
      row = 2; 
    }
    if((userPos == 1) || (userPos == 4) || (userPos == 7)){
      column = 0; 
    }
    if((userPos == 2) || (userPos == 5) || (userPos == 8)){
      column = 1; 
    }
    if((userPos == 3) || (userPos == 6) || (userPos == 9)){
      column = 2; 
    }
    
    } // ends while loop checking if the position is filled up 
    // if they entered a valid position, fill it with an "x" 
    array[row][column] = " x";
    return array;
  }
  
  public static boolean isWinner(String[][]array){ //accounts for all cases of 3 in a row 
    boolean winner = false; 
    if(((array[0][0].equals(array[0][1])) && (array[0][1].equals(array[0][2]))) /* first row the same*/ ||
      ((array[1][0].equals(array[1][1])) && (array[1][1].equals(array[1][2]))) /* second row the same*/||
      ((array[2][0].equals(array[2][1])) && (array[2][1].equals(array[2][2]))) /* third row the same*/ || 
      ((array[0][1].equals(array[1][1])) && (array[1][1].equals(array[2][1]))) /* second column same*/ ||
      ((array[0][0].equals(array[1][0])) && (array[1][0].equals(array[2][0]))) /* first column same*/ ||
      ((array[0][2].equals(array[1][2])) && (array[1][2].equals(array[2][2]))) /* third column same*/ ||
      ((array[0][0].equals(array[1][1])) && (array[1][1].equals(array[2][2]))) /* diagonal down right*/ ||
      ((array[0][2].equals(array[1][1])) && (array[1][1].equals(array[2][0]))) /* diagonal down left*/){
      winner = true; 
    }
    
    return winner; 
  }
  
  
} // ends public class