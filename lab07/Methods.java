///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 26 October 2018
/// This program generates a grammatically correct sentence 
import java.util.Scanner;
import java.util.Random; 
public class Methods{
  // create class to generate Adjectives 
  public static String adjective(){
    Random randGen = new Random();
    int adjNum = randGen.nextInt(10);
    String adj = " ";
    switch(adjNum){
      case 0: 
        adj = "skinny";
      case 1:
        adj = "fast";
        break;
      case 2: 
        adj = "slow";
        break;
      case 3: 
        adj = "chubby";
        break;
      case 4: 
        adj = "funny";
        break;
      case 5:
        adj = "miniature";
        break;
      case 6: 
        adj = "huge";
        break;
      case 7: 
        adj = "scary";
        break;
      case 8: 
        adj = "tiny";
        break;
      case 9: 
        adj = "massive";
        break;
    }
    return adj;
  } // ends adjective class
  
  // create a class to generate Subjects
  public static String subject(){
    Random randGen = new Random();
    int subNum = randGen.nextInt(10);
    String subj = " ";
    switch(subNum){
      case 0: 
        subj = "Dog";
      case 1:
        subj = "Man";
        break;
      case 2: 
        subj = "Fox";
        break;
      case 3: 
        subj = "Cat";
        break;
      case 4: 
        subj = "Elephant";
        break;
      case 5:
        subj = "Woman";
        break;
      case 6: 
        subj = "Armadillo";
        break;
      case 7: 
        subj = "Ardvark";
        break;
      case 8: 
        subj = "Mascot";
        break;
      case 9: 
        subj = "Lizard";
        break;
    }
    return subj;
  } // ends subjects class
  
  // create a class to generate past tense verbs
  public static String verb(){
    Random randGen = new Random();
    int verbNum = randGen.nextInt(10);
    String verb = " "; 
     switch(verbNum){
      case 0: 
        verb = "slapped";
      case 1:
        verb = "touched";
        break;
      case 2: 
        verb = "ran";
        break;
      case 3: 
        verb = "swam";
        break;
      case 4: 
        verb = "buried";
        break;
      case 5:
        verb = "destroyed";
        break;
      case 6: 
        verb = "employed";
        break;
      case 7: 
        verb = "helped";
        break;
      case 8: 
        verb = "reprimanded";
        break;
      case 9: 
        verb = "scolded";
        break;
    }
    return verb;
  } // ends verb class 
  
  // create object class 
  public static String object(){
    Random randGen = new Random();
    int obNum = randGen.nextInt(10);
    String ob = " ";
    switch(obNum){
      case 0: 
        ob = "plate";
      case 1:
        ob = "worker";
        break;
      case 2: 
        ob = "plane";
        break;
      case 3: 
        ob = "cabinet";
        break;
      case 4: 
        ob = "refrigerator";
        break;
      case 5:
        ob = "balloon";
        break;
      case 6: 
        ob = "computer";
        break;
      case 7: 
        ob = "speaker";
        break;
      case 8: 
        ob = "desk";
        break;
      case 9: 
        ob = "trash";
        break;
    } // ends switch 
    return ob; 
  } // ends object class 
  
  public static void sentence(){
    int k = 5;
    for(int i =1; i<=k; i++){
      if(i==1){
        System.out.print("The " + adjective());
      }
      else if (i ==2){
        System.out.print(" " + subject());
      }
      else if(i==3){
        System.out.print(" " + verb() + " ");
      }
      else if(i==4){
        System.out.print("the " + adjective());
      }
      else if(i==5){
        System.out.println(" " + object());
      }
    } // ends for loop
   
  } // ends sentence class
  
  
  public static String thesis(String subjPar){
    String thesis = ("The " + adjective() +" "+ subjPar +" "+ verb() +" the "+ adjective() +" "+ object() );
    return thesis;
  } // ends thesis method 
  
  public static String actionSentence(String subjPar){
     String actionSentence = (subjPar +" "+ verb() +" the " + adjective() +" "+ object()); 
    return actionSentence;
  } // ends action sentence method 
  
  public static String conclusion(String subjPar){
    String conclusion = ("That " + subjPar +" "+ verb() + " its " + subject());
    return  conclusion;
  }
  
  public static void paragraph(Scanner scnr, String subjPar){
    System.out.println("Please enter the number of sentences in the paragraph as an integer.");
    int numSen = scnr.nextInt();
    System.out.println(thesis(subjPar));
    for(int i=1; i<= numSen; i++){
      System.out.println(actionSentence(subjPar));
    } // ends for loop 
    System.out.println(conclusion(subjPar));
  } // ends paragraph class 
  
  public static void main(String[]args){
    Scanner scnr = new Scanner(System.in);
    String subjectPar = subject();
    int wantSentence = 1;
    while(wantSentence==1){
      sentence();
      System.out.println("Please enter 1 if you would like to generate another sentence.");
      wantSentence = scnr.nextInt();
    } // ends while loop 
    String subjPar = subject();
    paragraph(scnr, subjPar);
  } // end main method 
    
  }
