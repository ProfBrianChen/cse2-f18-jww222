///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 18 September 2018
/* This program determines the volume of a square pyramid based on base length and height */ 

import java.util.Scanner;
public class Pyramid{
   public static void main(String [] args){
     Scanner myScanner = new Scanner(System.in);
     
     // prompt the user to input base length and height as doubles 
     System.out.println("The square side of the pyramid is (input length): ");
     double a = myScanner.nextDouble(); 
     System.out.println("The height of the pyramid is (input height): ");
     double h = myScanner.nextDouble();
     
     // calculate the volume of the pyramid
     double volume = (Math.pow(a, 2)) * (h / 3);
     
     // print the volume to the terminal
     System.out.println("The volume of the pyramid is: " + volume);
     
   } // ends Main Method
} // ends public class 
