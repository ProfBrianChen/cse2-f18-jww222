///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 18 September 2018
/* This program determines the amount of rainfall during a given hurricane in cubic miles 
when given the acrage affected and average rainfall in inches */ 

import java.util.Scanner;
public class Convert{
  public static void main(String [] args){
    Scanner myScanner = new Scanner(System.in);
    
    
  //  Enter the affected area in acres: 23523.23
//Enter the rainfall in the affected area: 45
//0.02610444 cubic miles

    // Prompt the user to input the acrage as a double and the inches as an integer 
    System.out.println("Enter the affected area in acres: " );
    double acresAffected = myScanner.nextDouble();
    
    System.out.println("Enter the average rainfall in the affected area in inches: ");
    int inchesRain = myScanner.nextInt();
    
    // Compute to cubic miles
    double sqInchesInSqMile = Math.pow(5280 *12, 2); // amount of square inches in a square mile
    double gallsPerSqMile = (sqInchesInSqMile * inchesRain)/ 231; // converts to gallons per square mile 
    double sqMilesAffected = acresAffected * 0.0015625; // conversion factor multiplied by acresAffected gives sqMilesAffected
    double gallsTotalDropped = gallsPerSqMile * sqMilesAffected; // gives the amount of water dropped in the hurricane 
    double numGallsCubicMile = (Math.pow(5280, 3) * 7.4805198); // gives number of gallons in a cubic mile 
    double cubicMile = gallsTotalDropped / numGallsCubicMile; // gets amount of cubic miles of rain dropped 
    
    // print cubic miles to terminal
    System.out.println(cubicMile + " cubic miles");
    
    
  }
}