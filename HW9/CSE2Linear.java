///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 27 November 2018
/// This program searches an array using Linear and Binary searches. 
import java.util.Scanner;
import java.util.Arrays; 
import java.util.Random; 
public class CSE2Linear{
  
  public static int binary(int[] array, int key){
    
    // the following code is based on code from lecture notes "binarySearchDevelopment" on CourseSite
    int low = 0;
    int high = array.length-1;
    int counter = 0;
    while(high >= low) {
      int mid = (low + high)/2;
      if (key < array[mid]) {
        high = mid - 1;
        counter++; // denotes that an iteration has occurred
      }
      else if (key == array[mid]) {
        counter++;
        System.out.print("In " + counter + " iterations "); 
        return counter;
      }
      else {
        low = mid + 1;
        counter++; // denotes an iteration has occurred 
      }
    }
    System.out.print("In " + counter + " iterations "); 
    return -1; // if the number is not found, return the max number of iterations (4)
  } // ends binary search method
  
  public static void scramble (int[]array){
    Random random = new Random();
    for(int i = 0; i< 30; i++){ // use a for loop up to 30 (30 is arbitrary) to create 2 random index positions then 
                                // swap them to "scramble" the array
      int firstIndex = random.nextInt(15); // index from 0-14 
      int secondIndex = random.nextInt(15); // index from 0-14
      int temp = array[firstIndex];
      array[firstIndex] = array[secondIndex];
      array[secondIndex] = temp; 
    }
  } // ends scramble method
  
  public static int linearSearch(int[]array, int key){
    for(int i = 0; i<array.length; i++){
      if( key == array[i]){
        return i+1; 
      }
    }
    return -1; // if not found, then return -1 or a dummy value
  } // ends linear search method 
  
  public static void main(String[] args){
    Scanner scnr = new Scanner(System.in);
    Random random = new Random();
    int [] grades = new int [15]; // create array of size 15 to store the grades 
    System.out.println("Please input 15 integer grades in ascending order");
    int max = 0; // set the max  = 0 to have a way to check the user inputs in ascending order.
    // use for loop to prompt user to input the numbers
    for( int i = 0; i < grades.length; i++){
      boolean temp = scnr.hasNextInt();
      while(!temp){
        System.out.println("You have not input an integer. Only integers please.");
        scnr.next();
        temp = scnr.hasNextInt();
      }
      int tempNum = scnr.nextInt();
      // use while loop to ensure it's a valid grade value 
      while((tempNum < 1 ) || (tempNum>100)){
        System.out.println("You have not input a valid grade value. Only grades from 1-100 please.");
        scnr.next();
        temp = scnr.hasNextInt();
        while(!temp){
          System.out.println("You have not input an integer. Only integers please.");
          temp = scnr.hasNextInt();
        }
        tempNum = scnr.nextInt();
      } // ends while loop checking 1-100
      // ask user to enter a number that is greater than any previously entered numbers
      while(tempNum < max){
        System.out.println("Please only enter in ascending order. Enter a number higher than any previous number.");
        // make sure the user doesn't put a non integer in after
        temp = scnr.hasNextInt();
        while(!temp){
          System.out.println("You have not input an integer. Only integers please.");
          scnr.next();
          temp = scnr.hasNextInt();
        }
        tempNum = scnr.nextInt();
      }
      max = tempNum; // sets the max to be the temporary number
      grades[i] = tempNum; // finally sets the grades at that index location to the user entered number. 
    } // ends for loop creating the grades array 
    System.out.println(Arrays.toString(grades));
    System.out.println("Please input a specific grade you are looking for");
    int key = scnr.nextInt();
    
    // call binary to find number of iterations it took to find the key
    int binarySearch = binary(grades,key);
    if(binarySearch == -1){ // if the number wasn't found at all the binarySearch method will return -1  
      System.out.println(key + " was not found in the list");
    }
    else if(binarySearch > 0){ // if the number was found in the list binarySearch will return a # greater than 0 
      System.out.println(key + " was found in the list");
    }
    
    scramble(grades);
    System.out.println(Arrays.toString(grades));
    System.out.println("Please enter an integer to search for in the grades");
    key = scnr.nextInt();
    //linear search the scrambled array for a key 
    int linear = linearSearch(grades, key);
    if(linear == -1){
      System.out.println("In " + grades.length + " iterations " + key + " was not found");
    }
    else if (linear >0) {
      System.out.println("In " + linear + " iterations " + key + " was found");
    }
  } // ends main method
} // ends public class