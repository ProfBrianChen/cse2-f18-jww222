import java.util.Scanner;
import java.util.Random;
public class RemoveElements{
  
  public static int[] randomInput(){
    Random rand = new Random();
    int [] randArray = new int [10];
    for(int i = 0; i < randArray.length; i++){
      randArray[i] = rand.nextInt(10); 
    }
    return randArray;
  }// ends random input method
  
  public static int[] delete(int[] list, int pos){
    int[] newArray = new int[list.length-1];
    for(int i = 0; i < pos; i++){
      newArray[i] = list[i];
    } 
    for(int i = pos+1; i < list.length; i++){
      newArray[i-1] = list[i];
    }
    return newArray; 
  } // ends delete method
    
    public static int[] remove(int[]list, int target){
      // create for loop to count # of occurences of target to know the size of new array
      int counter = 0;
      for(int i = 0; i < list.length; i++){
        if(list[i] == target){
          counter++; } // increment each time it is found in the array
      }
      int [] newArray = new int[list.length-counter]; // creates array of proper size 
      // use a for each loop to compare values of the first array to the target
      int increment = 0; // set a counter to fill the new array
      for(int number : list){
        if(number != target){ // if the number value of the list is not the target value, put it into the new array
          newArray[increment] = number; 
          increment++; // increment to update new index position in the new array
        }
      }
      return newArray;
    }  // ends remove method

    public static void main(String [] arg){
      Scanner scan=new Scanner(System.in);
      int num[]=new int[10];
      int newArray1[];
      int newArray2[];
      int index,target;
      String answer="";
      do{
        System.out.print("Random input 10 ints [0-9]");
        num = randomInput();
        String out = "The original array is:";
        out += listArray(num);
        System.out.println(out);
        
        System.out.print("Enter the index ");
        index = scan.nextInt();
        newArray1 = delete(num,index);
        String out1="The output array is ";
        out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
        System.out.println(out1);
        
        System.out.print("Enter the target value ");
        target = scan.nextInt();
        newArray2 = remove(num,target);
        String out2="The output array is ";
        out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
        System.out.println(out2);
        
        System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
        answer=scan.next();
      }while(answer.equals("Y") || answer.equals("y"));
    }
    
    public static String listArray(int num[]){
      String out="{";
      for(int j=0;j<num.length;j++){
        if(j>0){
          out+=", ";
        }
        out+=num[j];
      }
      out+="} ";
      return out;
    }
  }
  
