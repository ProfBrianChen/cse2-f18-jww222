///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 21 September 2018
/// This program generates a random number then denotes which suit and type of card that number corresponds with 
import java.util.Scanner;

public class UserInput{
 public static void main(String[]args){ 
   Scanner scnr = new Scanner(System.in);
   
    // get the course number first
    System.out.println("Please input the course number as an integer.");
    boolean courseNum = scnr.hasNextInt();
     while (!courseNum){
       scnr.next();
       System.out.println("You did not enter an integer. Please try again.");
       courseNum = scnr.hasNextInt();
     }
    // convert value of courseNum as a 
    int a = scnr.nextInt();
   
    // get the user to input the number of weekly meetings 
    System.out.println("Please input the number of times the class meets weekly as an integer");
    boolean weeklyMeet = scnr.hasNextInt();
    while (!weeklyMeet){
      scnr.next();
      System.out.println("You have not entered an integer. Please try again");
      weeklyMeet = scnr.hasNextInt();
    }
    // update what the user inputted as the # of weekly meetings as b 
    int b = scnr.nextInt(); 
   
    // get the user to input the Department name as a string
    System.out.println("Please input the department name as a string");
    String depoName = scnr.next();
    
    // get the user to input the time the class starts in military time 
    System.out.println("Please input the time the class starts in military time, without a colon");
    boolean classTime = scnr.hasNextInt();
    while (!classTime){
      scnr.next();
      System.out.println("You have not input an integer. Please try again");
      classTime = scnr.hasNextInt(); 
    }
   
    // save the time clas starts as c 
    int c = scnr.nextInt();
   
    // get the user to input the instructor's name
    System.out.println("Please input your instructor's name");
    String instructor = scnr.next(); 
   
    // get the user to input the number of students in their class as an integer  
    System.out.println("Please input the number of students in the class as an integer.");
    boolean numStudents = scnr.hasNextInt();
    while (!numStudents){
      scnr.next();
      System.out.println("You have not input an integer. Please try again");
      numStudents = scnr.hasNextInt();
    }
   
    // save number of students as d 
    int d = scnr.nextInt(); 
    
   
   // print out all of the users inputs 
   System.out.println("The Course Number is: " + a);
   System.out.println("The department name is: " + depoName);
   System.out.println("The number of weekly meetings is: " + b);
   System.out.println("The class starts at " + c + " in military time.");
   System.out.println("The instructor's name is " + instructor);
   System.out.println("The number of students in class is " + d);

 } // ends main method
} // ends public class