///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 23 October 2018
/// This program creates a pattern forming an X of a given user length
import java.util.Scanner;
public class EncryptedX{
  public static void main(String[]args){
  Scanner scnr = new Scanner(System.in);
    //prompt user for # of stars in the pattern 
    System.out.println("Please input the amount of lines you would like in the pattern");
    boolean userVal = scnr.hasNextInt(); 
    
    // check that the user inputted an integer
    while(!userVal){
      System.out.println("You have not input an integer, please try again");
      userVal = scnr.hasNextInt();
    }
    int uVal = scnr.nextInt();
    
    // check that the user put a value in between 1 and 100  
    while((uVal< 1) || (uVal>100)){
      System.out.println("You have not input a number between 1 and 100. Please try again");
      uVal = scnr.nextInt();
    }
    
    // make the number of rows = the size of their grid 
    int numRows = uVal+1;
    
    for(int i=1; i<= numRows; i++)
    // first for loop creates the accurate number of rows 
    {
      for(int k=1; k <= numRows; k++)
      // second for loop creates accurate number of spaces and asterisks 
      {
        // print out spaces in appropriate spots
        if((k==i)|| (k==(numRows-(i-1)))){
          System.out.print(" ");
        } // ends IF to print spaces
        // print out asterisks in other places 
        else{
          System.out.print("*");
        }
      } // ends inner for loop
      System.out.println(" ");
    } // ends outter for loop 
    
   
  }
}