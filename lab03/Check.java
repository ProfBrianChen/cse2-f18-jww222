///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 13 September 2018
/// This file determines how much each person of a dinner party owes dependent on an inputed price and desired tip amount 

import java.util.Scanner;
public class Check{
   public static void main(String [] args){
     Scanner myScanner = new Scanner(System.in);
     
     // Get the user to input the exact cost of their original bill
     System.out.print("Enter the original cost of the check in the form xx.xx: ");
     double checkCost = myScanner.nextDouble();
     
     // Next, get the user to input their desired tip percentage
     System.out.print("Enter the  percentage tip that you wish to pay as a whole number (in the form xx): ");
     double tipPercent = myScanner.nextDouble();
     tipPercent /= 100;
     
     // Prompt the user to input the number of people at the dinner
     System.out.print("Enter the number of people who went out to dinner: ");
     int numPeople = myScanner.nextInt();
     
     // Calculate the total cost and amount paid per person then print to terminal 
     double totalCost;
     double costPerPerson;  
     int dollars;
     int dimes;
     int pennies;
     totalCost = checkCost * (1 + tipPercent);
     costPerPerson = totalCost / numPeople;
     dollars = (int) costPerPerson;
     dimes = (int) (costPerPerson * 10) % 10;
     pennies = (int) (costPerPerson * 100) % 10;
     System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies);

     

     
   }
}  