/////////////
////// CSE 002-110 {Joseph Wesselman}
///
public class WelcomeClass{
   public static void main(String [] args){
     // prints introduction to class to terminal 
     System.out.println("   -----------     ");
     System.out.println("   | WELCOME |     ");
     System.out.println("   -----------     ");
     System.out.println("   ^  ^  ^  ^  ^  ^  ");
     System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
     System.out.println("<-J--W--W--2--2--2->");
     System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
     System.out.println("  v  v  v  v  v  v  ");
 
   }
}
