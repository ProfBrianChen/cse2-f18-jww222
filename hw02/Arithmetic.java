///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 11 September 2018
/// This file computes and outputs prices and taxes on items in the state of Pennsylvania

public class Arithmetic{
   public static void main(String [] args){
      // Number of pairs of pants
      int numPants = 3;
      // Cost per pair of pants
      double pantsPrice = 34.98;
      // Number of sweatshirts
      int numShirts = 2;
      // Cost per shirt
      double shirtPrice = 24.99;
      // Number of belts
       int numBelts = 1;
      // cost per belt
      double beltPrice = 33.99;
      // the tax rate
      double paSalesTax = 0.06;
      // Sales tax of each item
      double salesTaxPants;
      double salesTaxShirts;
      double salesTaxBelts;
      // Total cost of each item
      double totalCostOfPants;
      double totalCostOfShirts;
      double totalCostOfBelts;
      // Total cost before Tax
      double totalCostBeforeTax;
      // Total Sales Tax 
      double totalSalesTax;
      // total cost of transaction 
      double totalCostAfterTax;
     
      // computes Sales Tax of all items 
      salesTaxPants = numPants * pantsPrice * paSalesTax;
      salesTaxPants = salesTaxPants * 100;
      salesTaxPants = (int) salesTaxPants;
      salesTaxPants = salesTaxPants / 100;
     
      salesTaxShirts = numShirts * shirtPrice * paSalesTax;
      salesTaxShirts = salesTaxShirts * 100;
      salesTaxShirts = (int) salesTaxShirts;
      salesTaxShirts = salesTaxShirts / 100; 
     
      salesTaxBelts = numBelts * beltPrice * paSalesTax;
      salesTaxBelts = salesTaxBelts * 100;
      salesTaxBelts = (int) salesTaxBelts;
      salesTaxBelts = salesTaxBelts / 100;
     
      // computes total cost of each item
      totalCostOfPants = numPants * pantsPrice * (1 + paSalesTax);
      totalCostOfPants = totalCostOfPants * 100;
      totalCostOfPants = (int) totalCostOfPants;
      totalCostOfPants = totalCostOfPants / 100; 
     
      totalCostOfShirts = numShirts * shirtPrice * (1 + paSalesTax);
      totalCostOfShirts = totalCostOfShirts * 100;
      totalCostOfShirts = (int) totalCostOfShirts;
      totalCostOfShirts = totalCostOfShirts / 100;
     
      totalCostOfBelts = numBelts * beltPrice * (1 + paSalesTax);
      totalCostOfBelts = totalCostOfBelts * 100;
      totalCostOfBelts = (int) totalCostOfBelts;
      totalCostOfBelts = totalCostOfBelts / 100;
     
      // computes total cost of transaction before tax 
      totalCostBeforeTax = (numPants * pantsPrice) + (numShirts * shirtPrice) + (numBelts * beltPrice);
      totalCostBeforeTax = totalCostBeforeTax * 100;
      totalCostBeforeTax = (int) totalCostBeforeTax;
      totalCostBeforeTax = totalCostBeforeTax / 100;
     
      // computes total sales tax
      totalSalesTax = salesTaxBelts + salesTaxShirts + salesTaxPants;
      totalSalesTax = totalSalesTax * 100;
      totalSalesTax = (int) totalSalesTax;
      totalSalesTax = totalSalesTax / 100;
     
      // computes total cost of transaction after taxes 
      totalCostAfterTax = totalCostOfBelts + totalCostOfShirts + totalCostOfPants;
      totalCostAfterTax = totalCostAfterTax * 100; 
      totalCostAfterTax = (int) totalCostAfterTax;
      totalCostAfterTax = totalCostAfterTax / 100;
     
      // prints Total Cost of each Item and the sales tax paid on them to terminal
      System.out.println("The total cost of the pants was " + totalCostOfPants);
      System.out.println("The sales tax paid on these pants was " + salesTaxPants);
      System.out.println("The total cost of the shirts was " + totalCostOfShirts );
      System.out.println("The sales tax paid on these shirts was " + salesTaxShirts);
      System.out.println("The total cost of the belts was " + totalCostOfBelts);
      System.out.println("The sales tax paid on these belts was " + salesTaxBelts);
     
      // prints Total Cost before tax to terminal
      System.out.println("The total cost before sales tax was " + totalCostBeforeTax);
      
      // prints out Total sales tax and the total cost of the transaction after taxes 
      System.out.println("The total sales tax on this transaction was " + totalSalesTax);
      System.out.println("The total cost of this transaction after tax was " + totalCostAfterTax);

  
     
     
   } // ends main method
} // ends public class 