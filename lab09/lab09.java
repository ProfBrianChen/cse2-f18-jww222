///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 16 November 2018
/// This program passes several arrays into methods and alters them
public class lab09{
  
  public static int[] copy(int[]array){
    int [] copiedArray = new int [array.length];
    for(int i=0; i<array.length; i++){ // copy the array passed into the method 
      copiedArray[i] = array[i];
    }
    return copiedArray;
  } // ends copy method
  
  public static void print(int[] array){
    for(int i =0; i<array.length; i++){
     System.out.print(array[i] + ", "); 
    }
    System.out.println();
  }
  
  public static int[] inverter(int[]array){
    int finalIndex = array.length-1;
    for(int i = 0; i < array.length/2; i++){
      int temp = array[i];
      array[i] = array[finalIndex-i];
      array[finalIndex-i] = temp;
    }
    return array; 
  }
  
  public static int[] inverter2(int[]array){
    int[] newArray = new int[array.length];
    newArray = copy(array);
    int finIndex = newArray.length-1;
    for(int i=0; i<newArray.length/2; i++){
      int temp = newArray[i];
      newArray[i] = newArray[finIndex-i];
      newArray[finIndex-i] = temp; 
    }
    return newArray;
  }
  
  public static void main(String[]args){
    int[]array0 = {1,3,5,7,9,11,13,15,17,19,21,23,25};
    int[] array1 = copy(array0);
    int[] array2 = copy(array0);
    inverter(array0);
    print(array0);
    inverter2(array1);
    print(array1);
    int [] array3 = inverter2(array2);
    print(array3);
  }// ends main method
} // ends public class