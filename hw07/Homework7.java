///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 30 October 2018
/// This program analyzes a given section of text
import java.util.Scanner;
public class Homework7{
  public static String sampleText(Scanner scnr){
    System.out.println("Please enter sample text");
    String sampleText = scnr.nextLine();
    return sampleText; 
  } // ends sampleText method
  
  public static int getNumOfNonWSCharacters(String sampleText){
    sampleText = sampleText.replaceAll("\\s","");
    int nonWS = sampleText.length();
    return nonWS; 
  } // ends nonWSCharacters method 
  
  public static int getNumOfWords(String sampleText){
    int numWords = 1;
    for(int i=0; i<sampleText.length(); i++){
      if(sampleText.charAt(i) == ' ' && sampleText.charAt(i+1) != ' '){
        numWords++;
      }
    }
    return numWords;
  } // ends numWords method 
  
  // FIXME
  public static int findText(Scanner scnr, String sampleText){
    System.out.println("Please input the word you'd like to search for");
    String wantedWord = scnr.next();
    int counter = 0; 
    int indexPosition = sampleText.indexOf(wantedWord);
    while(indexPosition >=0){
      indexPosition = sampleText.indexOf(wantedWord, indexPosition+1);
      counter++;
    }
    return counter;
  } // ends findText method 
  
  public static String replaceExclamation(String sampleText){
    String newSampleText = sampleText.replace("!",".");
    return newSampleText;
  } // ends replace exclamation method
  
  public static String shortenSpace(String sampleText){
    String oneSpaceText = sampleText.replaceAll(" +"," ");
    return oneSpaceText;
  }
  public static void printMenu(Scanner scnr, String sampleText){
    boolean dontQuit = true;
    while(dontQuit){
     System.out.println("Menu");
     System.out.println("c - Number of non-whitespace characters");
     System.out.println("w - Number of words");
     System.out.println("f - Find text");
     System.out.println("r - Replace all !'s");
     System.out.println("s - Shorten spaces");
     System.out.println("q - Quit");
    
     System.out.println("Please enter which option you would like, or select q to quit.");
     String userChoice = scnr.next();
      int notAChar = userChoice.length();
      while(notAChar>2){
        System.out.println("Please input one character.");
        userChoice = scnr.next();
        notAChar = userChoice.length();
      }
     char a = userChoice.charAt(0); 
      
      if(a == 'q'){
        break;
      }
      else if(a == 'c'){
        int nonWS = getNumOfNonWSCharacters(sampleText);
        System.out.println("The number of non-whitespace characters is: " + nonWS);
      }
      else if(a == 'w'){
       int numWords = getNumOfWords(sampleText);
       System.out.println("The number of words is: " + numWords);
      }
      else if(a == 'f'){
       int numOccurences = findText(scnr, sampleText);
       System.out.println("The number of occurences of this word is: " + numOccurences);
      }
      else if(a == 'r'){
        String newSampleText = replaceExclamation(sampleText);
        System.out.println("Edited Text: " + newSampleText);
      }
      else if(a == 's'){
       String oneSpaceText = shortenSpace(sampleText);
       System.out.println("Edited Text: " + oneSpaceText);
      }
      else {
        System.out.println("You have not chosen a valid option. Please try again.");
        userChoice = scnr.next();
        a = userChoice.charAt(0);
      }
    
    } // ends while loop
  }
  
  public static void main(String[]args){
    Scanner scnr = new Scanner(System.in);
    String sampleText = sampleText(scnr);
    System.out.println("You entered: " + sampleText);
    printMenu(scnr, sampleText);
  } // ends main method
  
} // ends public class