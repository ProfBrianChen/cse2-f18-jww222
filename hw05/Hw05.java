///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 9 October 2018
/// This program generates a given number of poker rounds and determines the probability of certain hands in that number of rounds  
import java.util.Scanner;

public class Hw05{
 public static void main(String[]args){ 
   Scanner scnr = new Scanner(System.in);
   int i, numFourKind = 0, numThreeKind = 0, numTwoPair = 0, numPair = 0; 
   boolean uValue; 
   
   // prompt user for the number of hands they want to generate 
   System.out.println("Please input the number of hands you would like to generate as an integer");
   uValue = scnr.hasNextInt();
   
   //check to see if they input an integer 
   while(!uValue){
     scnr.next();
     System.out.println("You have not input an integer, please try again.");
     uValue = scnr.hasNextInt();
   } // ends the check to see if user inputted an integer 
   int userVal = scnr.nextInt(); // the user's value is now stored as userVal 
   
  // System.out.println("Test userVal is " + userVal);
   
   for(i = 0; i < userVal; i++){
     
     // generate 5 random numbers, designating a hand in poker 
     double a1 = Math.random()*(52) +1;
     double b1 = Math.random()*(52) +1;
     double c1 = Math.random()*(52) +1;
     double d1 = Math.random()*(52) +1;
     double e1 = Math.random()*(52) +1;
     
     // explicitly cast them to integers
     int a = (int) a1;
     int b = (int) b1;
     int c = (int) c1; 
     int d = (int) d1;
     int e = (int) e1; 
     
     // Check to make sure they aren't the same card 
     while ((a == b) || (a == c) || (a == d) || (a == e) || (b == c) || (b == d) || (b == e) || (c == d) || (c == e) || (d == e)){
       // generate 5 random numbers, designating a hand in poker 
      a1 = Math.random()*(52) + 1;
      b1 = Math.random()*(52) + 1;
      c1 = Math.random()*(52) + 1;
      d1 = Math.random()*(52) + 1;
      e1 = Math.random()*(52) + 1;
     
     // explicitly cast them to integers
      a = (int) a1;
      b = (int) b1;
      c = (int) c1; 
      d = (int) d1;
      e = (int) e1; 
     } // ends while loop of checking if the program generated two of the same card
     
     // Modulus the generated integers to find the card value 
     a = a % 13;
     b = b % 13;
     c = c % 13;
     d = d % 13;
     e = e % 13;  
     
     // CONSIDER FULL HOUSE FIRST SO 
     while (((a==b)&&(b==c)&&(d==e)) || ((a==b)&&(b==d)&&(c==e)) || ((a==b)&&(b==e)&&(c==d)) || ((a==c)&&(c==d)&&(b==e)) || 
            ((a==c)&&(c==e)&&(b==d)) || ((a==d)&&(d==e)&&(c==b)) || ((b==c)&&(c==d)&&(a==e)) || ((b==d)&&(d==e)&&(a==c)) || 
            ((b==c)&&(c==e)&&(a==d)) || ((c==d)&&(d==e)&&(a==b))){
      
       numThreeKind++;
       numPair++;
       break;
     }
     // Use while loop to check the 5 cases when 4 of a kind occurs then increments 4 of a kind if conditions are met 
     while (((a==b)&&(b==c)&&(c== d)) || ((a==b)&&(b==c)&&(c==e)) || ((a==b)&&(b==e)&&(e==d)) 
         || ((a==c)&&(c==d)&&(d==e)) || ((e==b)&&(b==c)&&(c == d))){
       numFourKind++; 
       break;
     } // ends while loop to check for cases of 4 of a kind
     
     
     // use while loop to check for the 10 cases that would produce 3 of a kind 
     while (((a==b)&&(b==c)) || ((a==b)&&(b==d)) || ((a==b)&&(b==e)) || ((a==c)&&(c==d)) || ((a==c)&&(c==e))) {
       numThreeKind++;
       break;
     } // ends first 3 of a kind while loop checker
     while (((a==d)&&(d==e)) || ((b==c)&&(c==d)) || ((b==d)&&(d==e)) || ((b==c)&&(c==e)) || ((c==d)&&(d==e))){
       numThreeKind++;
       break;
     } // end second 3 of akind while loop checker 
     
     // check for the first 7 cases that would give two pair 
     while (((a==b)&&(c==d)) || ((a==b)&&(c==e)) || ((a==b)&&(d==e)) || ((a==c)&&(b==d)) || ((a==c)&&(b==e)) || 
            ((a==c)&&(d==e)) || ((a==d)&&(c==b))){
       numTwoPair++;
       break;
     } // ends first loop checker for two pair 
     // check for second 7 cases that would give two par
     while (((a==d)&&(c==e)) || ((a==d)&&(c==b)) || ((a==e)&& (b==c)) || ((a==e)&&(b==d)) || ((a==e)&&(d==e)) || 
            ((b==c)&&(e==d)) || ((c==d)&&(b==e))){
       numTwoPair++;
       break;
     } // ends second while loop checker for two pair 
     
     while ((a==b) || (a==c) || (a==d) || (a==e) || (b==c) || (b==d) || (b==e) || (c==d) || (c==e) || (d==e)){ 
      numPair++;
       break; 
     } // ends while loop for one pair  
     
   } // ends for statement that counts iterations of hands 
   System.out.println("You have generated " + userVal + " hands.");
   // compute and print the probaility of 4 of a kind to 3 decimal places 
   double probFourKind = (double) numFourKind / userVal;
  // System.out.println("TEST probFourKind: " + probFourKind);
   System.out.print("The probability of getting 4 of a kind in " + userVal + " hands is: ");
   System.out.printf( "%.3f",probFourKind);
   System.out.println(" ");
   // compute and print the probability of 3 of a kind to 3 decimal places 
   double probThreeKind = (double) numThreeKind / userVal;
   System.out.print("The probability of getting 3 of a kind in " + userVal + " hands is: ");
   System.out.printf("%.3f",probThreeKind);
   System.out.println(" ");
   // compute and print prob of getting 2 pairs to 3 decimal places. 
   double probTwoPair = (double) numTwoPair / userVal;
   System.out.print("The probability of getting Two Pair in " + userVal + " hands is: ");
   System.out.printf("%.3f",probTwoPair);
   System.out.println(" ");
   // compute and print out the prob of getting one pair to 3 decimal places 
   double probOnePair = (double) numPair / userVal;
   System.out.print("The probability of getting One Pair in " + userVal + " hands is: ");
   System.out.printf("%.3f",probOnePair);
   System.out.println(" ");
   
 } // ends main method
} // ends public class 