///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 14 November 2018
/// This program generates, prints, and shuffles a deck of cards, then 
import java.util.Scanner;
import java.util.Random;
public class hw08{ 
  
  public static void printArray(String[]list){
    // for loop that prints out an array holding card values 
    for(int i = 0; i< list.length; i++){
      System.out.print(list[i] + " ");
    }
    System.out.println();
  } // ends printArray method
  
  public static void shuffle(String[]list){
    Random random = new Random();
    //write a for loop that will swap values for a random index position and the first index position 
    for(int i = 0; i < 150; i++){
      int r = random.nextInt(list.length); // generates a random int to refer to a random index position 
                                           // in the given array 
      String temporary = list[0]; // holds value of index position one
      list[0] = list[r]; // swaps values
      list[r] = temporary; // assigns the random position with first positions value 
    }
  }// ends shuffle method
  
  public static String[] getHand(String [] list, int index, int numCards){
    String[] hand = new String[numCards]; // create new index to store the cards in the hand
    for(int t = 0; t<numCards; t++){ //for loop to go through hand index
      hand[t] = list[index]; // index is set to the end of cards index position, until the hand index is full 
                             // take cards from the back of the current deck 
      index--; 
    }
    return hand;
  }
  
  public static void main(String[] args) { 
    Scanner scnr = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 0; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13];  
    } 
    System.out.println();
    printArray(cards); 
    System.out.println("Shuffled");
    shuffle(cards); 
    printArray(cards); 
    while(again == 1){ 
      System.out.println("Enter the number of cards you want in the hand");
      numCards = scnr.nextInt();
      // check to ensure the user entered a valid number 
      while((numCards<1) || (numCards>52)){
        System.out.println("That is not a valid number of cards in a hand. Enter again.");
        numCards = scnr.nextInt();
      }
      System.out.println("Hand: ");
      hand = getHand(cards,index,numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scnr.nextInt(); 
    }  
  } // ends main method
} // ends public class
