///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 25 September 2018
/// This program determines if a special roll in craps has been made using if else statements

import java.util.Scanner;
public class CrapsIf{
  public static void main(String [] args){
    Scanner scnr = new Scanner (System.in);
    double randNum1; 
    double randNum2;  
    
    System.out.println("If you would like to input a specific roll, please type 1, if you would like a random roll to be generated, press 2");
    int userChoice = scnr.nextInt();
    if (userChoice == 1){
      System.out.println("Please input the number between 1 and 6 inclusive rolled on dice one");
      int userNum1 = scnr.nextInt();
      System.out.println("Please input the number between 1 and 6 inclusive rolled on dice two");
      int userNum2 = scnr.nextInt();
      // the if section uses the values inputed by the user and then prints out their slang name when rolled together
      
      // begins evaluating circumstances in which the dice rolled have the same value 
      if ((userNum1 == 1) && (userNum2 == 1)){
        System.out.println("You have rolled Snakeyes");
      } // ends subset if 
      else if ((userNum1 == 2) && (userNum2 == 2)){
        System.out.println("You have rolled Hard Four");
      } // ends else if 2 
      else if ((userNum1 == 3) && (userNum2 == 3)){
        System.out.println("You have rolled Hard Six");
      } // ends else if 3
      else if ((userNum1 == 4) && (userNum2 == 4)){
        System.out.println("You have rolled Hard Eight");
      } // ends else if 4
      else if ((userNum1 == 5) && (userNum2 == 5)){
        System.out.println("You have rolled Hard Ten");
      } // ends else if 5
      else if ((userNum1 == 6) && (userNum2 == 6)){
        System.out.println("You have rolled Boxcars");
      } // ends else if 6 
      
      // begins evaluating circumstances where the dice rolled were not identical 
      else if (((userNum1 == 1) && (userNum2 == 2)) || ((userNum1 == 2) && (userNum2 == 1))){ // if dice roll is a combination of 1 and 2 print out slang term Ace Deuce 
        System.out.println("You have rolled Ace Deuce");
      } // ends else if 7
      else if (((userNum1 == 1) && (userNum2 == 3)) || ((userNum1 == 3) && (userNum2 == 1))){ //  if dice roll is a combination of 1 and 3 print out slang term Easy Four
        System.out.println("You have rolled Easy Four");
      } // ends else if 8 
      else if (((userNum1 == 1) && (userNum2 == 4)) || ((userNum1 == 4) && (userNum2 == 1))){ // if dice roll is a combination of 1 and 4 print out slang term Fewer Five 
        System.out.println("You have rolled Fever Five");
      } // ends else if 9 
      else if (((userNum1 == 1) && (userNum2 == 5)) || ((userNum1 == 5) && (userNum2 == 1))){ // if dice roll is a combination of 1 and 5 print out slang term Easy Six
        System.out.println("You have rolled Easy Six");
      } // ends else if 10 
      else if (((userNum1 == 1) && (userNum2 == 6)) || ((userNum1 == 6) && (userNum2 == 1))){ // if dice roll is a combination of 1 and 6 print out slang term Seven Out
        System.out.println("You have rolled Seven Out");
      } // ends else if 11
      else if (((userNum1 == 3) && (userNum2 == 2)) || ((userNum1 == 2) && (userNum2 == 3))){ // if dice roll is a combination of 2 and 3 print out slang term Fewer Five
        System.out.println("You have rolled Fever Five");
      } // ends else if 12
      else if (((userNum1 == 2) && (userNum2 == 4)) || ((userNum1 == 4) && (userNum2 == 2))){
        System.out.println("You have rolled Easy Six");
      } // ends else if 13
      else if (((userNum1 == 2) && (userNum2 == 5)) || ((userNum1 == 5) && (userNum2 == 2))){ 
        System.out.println("You have rolled Seven Out");
      } // ends else if 14
      else if (((userNum1 == 2) && (userNum2 == 6)) || ((userNum1 == 6) && (userNum2 == 2))){
        System.out.println("You have rolled Easy Eight");
      } // ends else if 15 
      else if (((userNum1 == 3) && (userNum2 == 4)) || ((userNum1 == 4) && (userNum2 == 3))){
        System.out.println("You have rolled Seven Out");
      } // ends else if 16
      else if (((userNum1 == 3) && (userNum2 == 5)) || ((userNum1 == 5) && (userNum2 == 3))){
        System.out.println("You have rolled Easy Eight");
      } // ends else if 17
      else if (((userNum1 == 3) && (userNum2 == 6)) || ((userNum1 == 6) && (userNum2 == 3))){
        System.out.println("You have rolled Nine");
      } // ends else if 18
      else if (((userNum1 == 4) && (userNum2 == 5)) || ((userNum1 == 5) && (userNum2 == 4))){
        System.out.println("You have rolled Nine");
      } // ends else if 19
      else if (((userNum1 == 4) && (userNum2 == 6)) || ((userNum1 == 6) && (userNum2 == 4))){
        System.out.println("You have rolled Easy Ten");
      } // ends else if 20 
      else if (((userNum1 == 5) && (userNum2 == 6)) || ((userNum1 == 6) && (userNum2 == 5))){
        System.out.println("You have rolled Yo-leven");
      } // ends else if 21
      else {
        System.out.println("You have input an invalid dice combination");
      }
    } // ends original if 
    else if (userChoice == 2) {
      randNum1 = 1 + Math.random()*(6);// generates random number between 1 and 6 inclusive 
      randNum1 = (int) randNum1; // explicitly casts the double output by Math.random to an integer
      randNum2 = 1 + Math.random()*(6); // generates random number bewteen 1 and 6 inclusive 
      randNum2 = (int) randNum2; // explicitly casts the double output by Math.random to an integer  
      // Begin by determining if the random numbers generated are equal to each other 
      
      if ((randNum1 == 1) && (randNum2 == 1)){
        System.out.println("You have rolled two 1's, also called Snakeyes");
      } // ends subset if 
      else if ((randNum1 == 2) && (randNum2 == 2)){
        System.out.println("You have rolled two 2's, also called Easy Four");
      } // ends else if 1 
      else if ((randNum1 == 3) && (randNum2 == 3)){
        System.out.println("You have rolled two 3's, also called Hard Six");
      } // ends else if 2
      else if ((randNum1 == 4) && (randNum2 == 4)){
        System.out.println("You have rolled two 4's, also called Hard Eight");
      } // ends else if 3
      else if ((randNum1 == 5) && (randNum2 == 5)){
        System.out.println("You have rolled two 5's, also called Hard Ten");
      } // ends else if 4
      else if ((randNum1 == 6) && (randNum2 == 6)){
        System.out.println("You have rolled two 6's, also called Boxcars");
      } // ends else if 5
      
      // next, determine the cases in which the numbers rolled are different 
      else if (((randNum1 == 1) && (randNum2 == 2)) || ((randNum1 == 2) && (randNum2 == 1))){ // if dice roll is a combination of 1 and 2 print out slang term Ace Deuce 
        System.out.println("You have rolled a 1 and a 2, also called Ace Deuce");
      } // ends else if 6
      else if (((randNum1 == 1) && (randNum2 == 3)) || ((randNum1 == 3) && (randNum2 == 1))){ // if dice roll is a combination of 1 and 3 print out slang term Easy Four 
        System.out.println("You have rolled a 1 and a 3, also called Easy Four");
      } // ends else if 7
      else if (((randNum1 == 1) && (randNum2 == 4)) || ((randNum1 == 4) && (randNum2 == 1))){ // if dice roll is a combination of 1 and 4 print out slang term Fewer Five 
        System.out.println("You have rolled a 1 and a 4, also called Fever Five");
      } // ends else if 8 
       else if (((randNum1 == 1) && (randNum2 == 5)) || ((randNum1 == 5) && (randNum2 == 1))){ // if dice roll is a combination of 1 and 5 print out slang term Easy Six
        System.out.println("You have rolled a 1 and a 5, also called Easy Six");
      } // ends else if 11 
      else if (((randNum1 == 1) && (randNum2 == 6)) || ((randNum1 == 6) && (randNum2 == 1))){ // if dice roll is a combination of 1 and 6 print out slang term Seven Out
        System.out.println("You have rolled a 1 and a 6, also called Seven Out");
      } // ends else if 9
      else if (((randNum1 == 2) && (randNum2 == 3)) || ((randNum1 == 3) && (randNum2 == 2))){ // if dice roll is a combination of 2 and 3 print out slang term Fewer Five
        System.out.println("You have rolled a 2 and a 3, also called Fever Five");
      } // ends else if 10
      else if (((randNum1 == 2) && (randNum2 == 4)) || ((randNum1 == 4) && (randNum2 == 2))){ // if dice roll is a combination of 2 and 4 print out slang term Easy Six
        System.out.println("You have rolled a 2 and a 4, also called Easy Six");
      } // ends else if 11 
       else if (((randNum1 == 2) && (randNum2 == 5)) || ((randNum1 == 5) && (randNum2 == 2))){ // if dice roll is a combination of 2 and 5 print out slang term Seven Out
        System.out.println("You have rolled a 2 and a 5, also called Seven Out");
      } // ends else if 12
       else if (((randNum1 == 2) && (randNum2 == 6)) || ((randNum1 == 6) && (randNum2 == 2))){ // if dice roll is a combination of 2 and 6 print out slang term Easy Eight
        System.out.println("You have rolled a 2 and a 6, also called Easy Eight");
      } // ends else if 13
       else if (((randNum1 == 3) && (randNum2 == 4)) || ((randNum1 == 4) && (randNum2 == 3))){ // if dice roll is a combination of 3 and 4 print out slang term Seven Out 
        System.out.println("You have rolled a 3 and a 4, also called Seven Out");
      } // ends else if 14 
       else if (((randNum1 == 3) && (randNum2 == 5)) || ((randNum1 == 5) && (randNum2 == 3))){ // if dice roll is a combination of 3 and 5 print out slang term Easy Eight
        System.out.println("You have rolled a 3 and a 5, also called Easy Eight");
      } // ends else if 15
       else if (((randNum1 == 3) && (randNum2 == 6)) || ((randNum1 == 6) && (randNum2 == 3))){ // if dice roll is a combination of 3 and 6 print out slang term Nine
        System.out.println("You have rolled a 2 and a 4, also called Nine");
      } // ends else if 16
       else if (((randNum1 == 5) && (randNum2 == 4)) || ((randNum1 == 4) && (randNum2 == 5))){ // if dice roll is a combination of 4 and 5 print out slang term Nine 
        System.out.println("You have rolled a 4 and a 5, also called Nine");
      } // ends else if 17 
       else if (((randNum1 == 4) && (randNum2 == 6)) || ((randNum1 == 6) && (randNum2 == 4))){ // if dice roll is a combination of 4 and 6 print out slang term Easy Ten
        System.out.println("You have rolled a 6 and a 4, also called Easy Ten");
      } // ends else if 18 
       else if (((randNum1 == 5) && (randNum2 == 6)) || ((randNum1 == 6) && (randNum2 == 5))){ // if dice roll is a combination of 5 and 6 print out slang term Yo-leven 
        System.out.println("You have rolled a 5 and a 6, also called Yo-leven");
      } // ends else if 19 
      else {
        System.out.println("The random number generator generated an invalid dice combination");
      } // ends subset else 
    } // ends original else if 
    else {
      System.out.println("You have not indicated a valid input for random or user dice combinations");
    }
      
    
    
  } // ends main method

} // ends public class 