///////////
// Joseph Wesselman
// CSE 002-011-FL18
// 25 September 2018
/// This program determines if a special roll in craps has been made using switch statements

import java.util.Scanner;
public class CrapsSwitch{
  public static void main(String [] args){
    Scanner scnr = new Scanner (System.in);
    
    // prompt user to enter 1 if they want to input dice values or any other integer if they want random values generated
    System.out.println("Enter 1 if you would like to input values for a craps roll, or enter 2 to have random numbers generated");
    int userChoice = scnr.nextInt();
    scnr.nextLine();
    
    switch (userChoice){
      case 1: 
        System.out.println("Please enter the value of dice one from 1-6 inclusive");
        int userNum1 = scnr.nextInt();
        scnr.nextLine(); // makes it so java ignores the enter you hit after inputting the value 
        System.out.println("Please enter the value of dice two from 1-6 inclusive");
        int userNum2 = scnr.nextInt();
        scnr.nextLine(); // makes java ignore the enter after you hit the input value
                
        switch (userNum1){
          case 1: // when userNum1 = 1 
             switch (userNum2){
               case 1:
                 System.out.println("You have rolled Snakeyes");
                 break;
               case 2:
                 System.out.println("You have rolled Ace Deuce");
                 break;
               case 3:
                 System.out.println("You have rolled Easy Four");
                 break;
               case 4:
                 System.out.println("You have rolled Fever Five");
                 break;
               case 5:
                 System.out.println("You have rolled Easy Six");
                 break;
               case 6:
                 System.out.println("You have rolled Seven Out");
                 break;
               default:
                 System.out.println("You have input an invalid value for a six sided dice");
               } // ends switch for userNum1 = 1 
            break;
          case 2: // when userNum1 = 2 
            switch (userNum2){
              case 1: 
                System.out.println("You have rolled Ace Deuce");
                break;
              case 2:
                System.out.println("You have rolled Hard Four");
                break;
              case 3:
                System.out.println("You have rolled Fever Five");
                break;
              case 4:
                System.out.println("You have rolled Easy Six");
                break;
              case 5:
                System.out.println("You have rolled Seven Out");
                break;
              case 6:
                System.out.println("You have rolled Easy Eight");
                break;
              default:
                System.out.println("You have input an invalid value for a six sided dice");          
            } // ends switch for userNum1 = 2
            break; // breaks when dice 1 = 2  
          case 3: // when userNum1 = 3 
             switch (userNum2){
               case 1: 
                 System.out.println("You have rolled Easy Four");
                 break;
               case 2: 
                 System.out.println("You have rolled Fever Five");
                 break; 
               case 3:
                 System.out.println("You have rolled Hard Six");
                 break;
               case 4: 
                 System.out.println("You have rolled Seven Out");
                 break;
               case 5:
                 System.out.println("You have rolled Easy Eight");
                 break;
               case 6:
                 System.out.println("You have rolled Nine");
                 break;
               default:
                  System.out.println("You have input an invalid value for a six sided dice");            
             } // ends switch for userNum1 = 3
            break;
          case 4: //when userNum1 = 4
            switch (userNum2){
              case 1:
                System.out.println("You have rolled Fever Five");
                break;
              case 2: 
                System.out.println("You have rolled Easy Six");
                break;
              case 3: 
                System.out.println("You have rolled Seven Out");
                break;
              case 4:
                System.out.println("You have rolled Hard Eight");
                break;
              case 5:
                System.out.println("You have rolled Nine");
                break;
              case 6:
                System.out.println("You have rolled Easy Ten");
                break;
              default:
                  System.out.println("You have input an invalid value for a six sided dice");               
            } // ends switch for userNum1 = 4 
            break;
          case 5: // when userNum1 = 5
            switch (userNum2){
              case 1:
                System.out.println("You have rolled Easy Six");
                break;
              case 2:
                System.out.println("You have rolled Seven Out");
                break;
              case 3:
                System.out.println("You have rolled Easy Eight");
                break;
              case 4:
                System.out.println("You have rolled Nine");
                break;
              case 5: 
                System.out.println("You have rolled Hard Ten");
                break;
              case 6:
                System.out.println("You have rolled Yo-leven");
                break;
              default:
                System.out.println("You have input an invalid value for a six sided dice");                
            } // ends switch for userNum1 = 5 
            break;
          case 6: 
           switch (userNum2){
              case 1:
                System.out.println("You have rolled Seven Out");
                break;
              case 2:
                System.out.println("You have rolled Easy Eight");
                break;
              case 3:
                System.out.println("You have rolled Nine");
                break;
              case 4:
                System.out.println("you have rolled Easy Ten");
                  break;
                case 5:
                  System.out.println("You have rolled Yo-leven");
                  break;
                case 6:
                  System.out.println("You have rolled Boxcars");
                  break;
                default:
                  System.out.println("You have input an invalid value for a six sided dice");
        } // ends switch for userNum1 = 6  
            break;
        } // ends switch for the first dice 
        break; // break in between case 1 and 2 of Original switch 
      case 2:
        int randNum1; 
        int randNum2;
        randNum1 = (int) (1 + Math.random()*(6));
        // randNum1 = (int) randNum1;
        randNum2 = (int) (1 + Math.random()*(6));
        // randNum2 = (int) randNum2;
        int e = randNum1;
        int f = randNum2;
        
        switch (e){
          case 1:
            switch (f){
              case 1:
                System.out.println("You have rolled two 1's, also called Snakeyes");
                break;
              case 2:  
                System.out.println("You have rolled a 1 and a 1, also called Ace Deuce");
                break;
              case 3:
                System.out.println("You have rolled a 1 and a 3, also called Easy Four");
                break;
              case 4:
                System.out.println("You have rolled a 1 and a 4, also called Fever Five");
                break;
              case 5: 
                System.out.println("You have rolled a 1 and a 5, also called Easy Six"); 
                break;
              case 6:
                System.out.println("You have rolled a 1 and a 6, also called Seven Out");
                break;
              default:
                System.out.println("The random number generator has created an invalid value for a six sided dice");
            } // ends switch for when randNum1 = 1
          break; // breakes for case 1 when randNum1 = 1 
          case 2: 
            switch (f){
              case 1:
                System.out.println("You have rolled a 1 and a 2, also called Ace Deuce");
                break;
              case 2: 
                System.out.println("You have rolled two 2's, also called Hard Four");
                break;
              case 3:
                System.out.println("You have rolled a 2 and a 3, also called Fever Five");
                break;
              case 4:
                System.out.println("You have rolled a 2 and a 4, also called Easy Six");
                break;
              case 5: 
                System.out.println("You have rolled a 2 and a 5, also called Seven Out");
                break;
              case 6:
                System.out.println("You have rolled a 2 and a 6, also called Easy Eight");
                break;
              default:
                  System.out.println("The random number generator has created an invalid value for a six sided dice");     
            } // ends switch for when randNum1 = 2 
          break; // breaks for case 2 when randNum1 = 2 
          case 3: 
            switch (f){
            case 1: 
              System.out.println("You have rolled a 1 and a 3, also called Easy Four");
              break;
            case 2:
              System.out.println("You have rolled a 2 and a 3, also called Fever Five ");
              break;
            case 3:
              System.out.println("You have rolled a 3 and a 3, also called Hard Six");
              break;
            case 4: 
              System.out.println("You have rolled a 3 and a 4, also called Seven Out");
              break;
            case 5:
              System.out.println("You have rolled a 3 and a 5, also called Easy Eight");
              break;
            case 6:
              System.out.println("You have rolled a 3 and a 6, also called Nine");
              break;
            default:
              System.out.println("The random number generator has created an invalid value for a six sided dice");     
            } // ends switch for when randNum = 3 
          break;
          case 4: 
            switch (f){
              case 1:
                System.out.println("You have rolled a 1 and a 4, also called Fever Five");
                break;
              case 2: 
                System.out.println("You have rolled a 2 and a 4, also called Easy Six");
                break;
              case 3: 
                System.out.println("You have rolled a 3 and a 4, also called Seven Out");
                break;
              case 4:
                System.out.println("You have rolled two 4's, also called Hard Eight");
                break;
              case 5:
                System.out.println("You have rolled a 4 and a 5, also called Nine");
                break;
              case 6:
                System.out.println("You have rolled a 4 and a 6, also called Easy Ten");
                break;
              default:
                  System.out.println("The random number generator has created an invalid value for a six sided dice");
            } // ends switch for when randNum1 = 4 
          break;
          case 5:
            switch (f){
              case 1:
                System.out.println("You have rolled a 1 and a 5, also called Easy Six");
                break;
              case 2:
                System.out.println("You have rolled a 2 and a 5, also called Seven Out");
                break;
              case 3:
                System.out.println("You have rolled a 3 and a 5, also called Easy Eight");
                  break;
                case 4:
                  System.out.println("You have rolled a 4 and a 5, also called Nine");
                  break;
                case 5: 
                  System.out.println("You have rolled two 5's, also called Hard Ten");
                  break;
                case 6:
                  System.out.println("You have rolled a 5 and a 6, also called Yo-leven");
                  break;
                default:
                  System.out.println("The random number generator has created an invalid value for a six sided dice");
            } // ends switch for wehen randnum1 = 5 
          break;
          case 6: 
            switch (f){
              case 1:
                System.out.println("You have rolled a 1 and a 6, also called Seven Out");
                break;
              case 2:
                System.out.println("You have rolled a 2 and a 6, also called Easy Eight");
                break;
              case 3:
                System.out.println("You have rolled a 3 and a 6, also called Nine");
                break;
              case 4:
                System.out.println("you have rolled a 4 and a 6, also called Easy Ten");
                break;
              case 5:
                System.out.println("You have rolled a 5 and 6, also called Yo-leven");
                break;
              case 6:
                System.out.println("You have rolled two 6's, also called Boxcars");
                break;
              default:
                System.out.println("The random number generator has created an invalid value for a six sided dice");
  
            } // ends switch for when randNum1 = 6 
          break;
          default:
            System.out.println("The Number Generator generated values that are invalid for a six sided dice");
        } // ENDS First switch w/ random inputs 
        break;
       
      default: 
        System.out.println("You have selected an invalid option. Please run again and enter one or two depending on your desired outcome");

    
    
  } // ends switch with userChoice
} // ends main method
} // ends public class